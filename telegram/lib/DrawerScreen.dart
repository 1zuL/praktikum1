import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          const UserAccountsDrawerHeader(
            accountName: Text("1zuL"),
            currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("assets/images/12.jpeg")),
            accountEmail: Text("ozuxekyosuke@gmail.com"),
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: "Grup Baru",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: "Kontak",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: "Panggilan",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: "Pengguna Sekitar",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: "Pesan Tersimpan",
            onTilePressed: () {},
          ),
          DrawerListTile(
            iconData: Icons.group,
            title: "Pengaturan",
            onTilePressed: () {},
          ),
        ],
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final VoidCallback onTilePressed;
  const DrawerListTile(
      {Key? key,
      required this.iconData,
      required this.title,
      required this.onTilePressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title,
        style: const TextStyle(fontSize: 16),
      ),
    );
  }
}
