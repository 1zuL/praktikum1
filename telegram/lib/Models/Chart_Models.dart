class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'Renata',
      message: 'Kamu Kemana aja si jul??',
      time: '12.00',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'vivi',
      message: 'nanti malam ada acara kemana jull??',
      time: '9 march',
      profileUrl:
          'https://assets.pikiran-rakyat.com/crop/8x9:492x535/x/photo/2022/01/09/711091793.png'),
  ChartModel(
      name: 'kayes',
      message: 'Mau seblak :(',
      time: '10 march',
      profileUrl:
          'https://blue.kumparan.com/image/upload/v1640238165/qrlapcahuaqerwe6chvc.webp'),
  ChartModel(
      name: 'vior',
      message: 'Mo nangisss :(((',
      time: '12.21',
      profileUrl:
          'https://media.suara.com/pictures/653x366/2022/02/10/29551-nita-vior-instagramnitavior.jpg'),
];
