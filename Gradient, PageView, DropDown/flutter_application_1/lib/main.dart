import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(new MaterialApp(
    home: new Home(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // @override
  // Widget build(BuildContext context) {
  //   return new Scaffold(
  //     body: new Container(
  //       decoration: new BoxDecoration(
  //           gradient: new LinearGradient(
  //               begin: FractionalOffset.topCenter,
  //               end: FractionalOffset.bottomCenter,
  //               colors: [
  //             Colors.white,
  //             Colors.purpleAccent,
  //             Colors.deepPurple
  //           ])),
  //     ),
  //   );
  // }

  final List<String> gambar = ["pp.jpg", "2.jpeg", "3.jpg", "4.jpeg"];
  static const Map<String, Color> colors = {
    '1': Color(0xFF2DB569),
    '2': Color.fromARGB(255, 181, 45, 45),
    '3': Color.fromARGB(255, 231, 154, 38),
    '4': Color.fromARGB(255, 77, 181, 45),
    '5': Color.fromARGB(255, 34, 174, 192),
    '6': Color.fromARGB(255, 59, 45, 181),
  };
  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return new Scaffold(
      body: new Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
                  Colors.white,
                  Color.fromARGB(255, 31, 31, 31),
                  Color.fromARGB(255, 173, 173, 173)
                ]),
          ),
          child: new PageView.builder(
              controller: new PageController(viewportFraction: 0.8),
              itemCount: gambar.length,
              itemBuilder: (BuildContext context, int i) {
                return new Padding(
                    padding: new EdgeInsets.symmetric(
                        horizontal: 5.0, vertical: 50.0),
                    child: new Material(
                      elevation: 8.0,
                      child: new Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          new Hero(
                            tag: gambar[i],
                            child: new Material(
                              child: new InkWell(
                                child: new Flexible(
                                  flex: 1,
                                  child: Container(
                                    color: colors.values.elementAt(i),
                                    child: new Image.asset(
                                      "img/${gambar[i]}",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                onTap: () => Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (BuildContextcontext) =>
                                            new Halamandua(
                                              gambar: gambar[i],
                                              colors:
                                                  colors.values.elementAt(i),
                                            ))),
                              ),
                            ),
                          )
                        ],
                      ),
                    ));
              })),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  _HalamanduaState createState() => _HalamanduaState();
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Blue", warna: Colors.blue),
  const Pilihan(teks: "Green", warna: Colors.green),
];

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.grey;
  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Choose Your Fighter"),
          backgroundColor: Color.fromARGB(255, 66, 66, 66),
          actions: <Widget>[
            new PopupMenuButton<Pilihan>(
              onSelected: _pilihannya,
              itemBuilder: (BuildContext context) {
                return listPilihan.map((Pilihan x) {
                  return new PopupMenuItem<Pilihan>(
                    child: new Text(x.teks),
                    value: x,
                  );
                }).toList();
              },
            )
          ],
        ),
        body: new Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                  gradient: new RadialGradient(
                      center: Alignment.center,
                      colors: [
                    Color.fromARGB(255, 255, 255, 255),
                    warna,
                    Color.fromARGB(255, 100, 100, 100)
                  ])),
            ),
            new Center(
              child: new Hero(
                tag: widget.gambar,
                child: new ClipOval(
                  child: new SizedBox(
                    width: 200.0,
                    height: 200.0,
                    child: new Material(
                      child: new InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: new Flexible(
                          flex: 1,
                          child: Container(
                            color: widget.colors,
                            child: new Image.asset(
                              "img/${widget.gambar}",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
